APRES RESTRUCT V2

d�pendance : aucune

POSITION NOM POKEMON DANS POKEMON_STATUS
Dans Pokemon_Status :
Remplacez les : 
@pokemon_window.draw_text(86, -9, 186, 39, @pokemon.given_name)
Par :
@pokemon_window.draw_text(95, -9, 186, 39, @pokemon.given_name)

CRASH METEO
Dans Pokemon_Battle_Pre_Post_Rounds l.177 :
Remplacez :
# Pluie
      pluie(list)
      # Ensoleill�
      soleil(list)
      # Tempete de sable
      tempete_sable(list)
      # Gr�le
      grele(list)
	  
Par :
# Pluie
      pluie(list, count)
      # Ensoleill�
      soleil(list, count)
      # Tempete de sable
      tempete_sable(list, count)
      # Gr�le
      grele(list, count)
	  
Dans Pokemon_Methods_Pre_Post_Rounds
Remplacez successivement :
def pluie(list)
def soleil(list)
def tempete_sable(list)
def grele(list)

Par :
def pluie(list, count)
def soleil(list, count)
def tempete_sable(list, count)
def grele(list, count)

Probleme daffichage menu l. 1487 :
Remplacez def self.update_library et son contenu par :
def self.update_library
    $picture_data = {}
    directory_list = ["Anime/Front_Male/", "Anime/Front_Female/", 
                      "Anime/Back_Male/", "Anime/Back_Female/",
                      "Shiny_Anime/Front_Male/", "Shiny_Anime/Front_Female/", 
                      "Shiny_Anime/Back_Male/", "Shiny_Anime/Back_Female/", 
                      "Icon/", "Eggs/", "Front_Male/", "Front_Female/",
                      "Shiny_Front_Male/", "Shiny_Front_Female/"]
    for name in directory_list    
      directory = explore("Graphics/Battlers/#{name}")
      directory[0].each do |file|
        $picture_data["Graphics/Battlers/#{name}#{file}"] = true
      end
    end
  end


Dans Scene_Title de Syst�me G�n�ral (� renommer Scene_Title_SG)  l.19 :
Remplacez :
if $game_system == nil
      $data_actors        = load_data("Data/Actors.rxdata")
      $data_classes       = load_data("Data/Classes.rxdata")
      $data_skills        = load_data("Data/Skills.rxdata")
      $data_items         = load_data("Data/Items.rxdata")
      $data_weapons       = load_data("Data/Weapons.rxdata")
      $data_armors        = load_data("Data/Armors.rxdata")
      $data_enemies       = load_data("Data/Enemies.rxdata")
      $data_troops        = load_data("Data/Troops.rxdata")
      $data_states        = load_data("Data/States.rxdata")
      $data_animations    = load_data("Data/Animations.rxdata")
      $data_tilesets      = load_data("Data/Tilesets.rxdata")
      $data_common_events = load_data("Data/CommonEvents.rxdata")
      $data_system        = load_data("Data/System.rxdata")
      $picture_data       = load_data("Data/Library.rxdata")
      
      $game_system = Game_System.new
    end
Par :
if $game_system == nil
      filenames = %w[Actors Classes Skills Items Weapons Armors Enemies Troops
                     States Animations Tilesets CommonEvents System]
      loaded = []
      threadLibrary = Thread.new { Scene_Debug.update_library }
      threads = filenames.map do |filename|
        next Thread.new do
          loaded[filenames.index(filename)] = load_data("Data/#{filename}.rxdata")
        end
      end
      threads.each { |thread| thread.join }
      threadLibrary.join
      $data_actors = loaded[0]
      $data_classes = loaded[1]
      $data_skills = loaded[2]
      $data_items = loaded[3]
      $data_weapons = loaded[4]
      $data_armors = loaded[5]
      $data_enemies = loaded[6]
      $data_troops = loaded[7]
      $data_states = loaded[8]
      $data_animations = loaded[9]
      $data_tilesets = loaded[10]
      $data_common_events = loaded[11]
      $data_system = loaded[12]
      $game_system = Game_System.new
    end
	
l.77 :
Remplacez :
for i in 0..2
      if FileTest.exist?("Save#{i+1}.rxdata")
        number += 1
      end
    end
	
Par :
1.upto(MAX_SAUVEGARDES) do |i|
      number += 1 if FileTest.exist?("Save#{i}.rxdata")
    end
	
l.338 :
Remplacez :
if SAVEBOUNDSLOT and @number == 3
Par :
if @number == MAX_SAUVEGARDES

l.323 :
Remplacez :
@index += @index == @number ? 0 : 1
      if @erase and @index > 2
        @index = 2
      end	  
Par :
@index += (@index >= @number) ? 0 : 1
	
Dans Pokemon_Data l.615 :
Supprimez :
if FileTest.exist?("Graphics/Battlers")
    picture_data = {}
    directory_list = ["Anime/Front_Male/", "Anime/Front_Female/", 
                      "Anime/Back_Male/", "Anime/Back_Female/",
                      "Shiny_Anime/Front_Male/", "Shiny_Anime/Front_Female/", 
                      "Shiny_Anime/Back_Male/", "Shiny_Anime/Back_Female/", 
                      "Icon/", "Eggs/", "Menu/" "Menu/Female/",
                      "Shiny_Menu/", "Shiny_Menu/Female"]
    directory_list.each do |name|    
      directory = explore("Graphics/Battlers/#{name}")
      directory[0].each do |file|
        picture_data["Graphics/Battlers/#{name}#{file}"] = true
      end
    end
      
    library = File.open("Data/Library.rxdata", "wb")
    Marshal.dump(picture_data, library)
    library.close
  end  
	
Vous pouvez supprimer le fichier Library.rxdata dans le dossier "Data" qui ne sert plus � rien
	
l.222 :
En-dessous de :
# -----------------------------------------------------------------
      #     Fin de la sc�ne d'intro
      # -----------------------------------------------------------------
Ajoutez :
thr.join

Dans Pokemon_Methods_Interface :
Supprimez def battler_menu et son contenu
Remplacez def battler_form et son contenu par :
def battler_form
      resultat = ""
      if @form == nil
        @form = 0
      end
      if @form > 0
        if @form < 10
          resultat = sprintf("_0%d", @form)
        else
          resultat = sprintf("_%d", @form)
        end
      end
      return resultat
    end
	
Remplacez def battler_face et son contenu par :
def battler_face(anime = true)
      ida = sprintf("%03d", id)
      prefixe = anime ? "Anime/" : ""
       
      if @egg
        string = "Eggs/#{ida}.png"
        if not( $picture_data["Eggs/#{string}"] )
          string = "Eggs/Egg000.png"
        end
        return string
      end
      
      if @gender == 1 or @gender == 0
        string = "#{prefixe}Front_Male/#{ida}#{battler_form}.png"
      elsif @gender == 2
        string = "#{prefixe}Front_Female/#{ida}#{battler_form}.png"
        if not($picture_data["Graphics/Battlers/#{string}"] )
          string = "#{prefixe}Front_Male/#{ida}#{battler_form}.png"
        end
      end
             
      if @shiny
        string2 = "Shiny_#{string}"
        if $picture_data["Graphics/Battlers/#{string2}"]
          string = string2
        end
      end
       
      if not( $picture_data["Graphics/Battlers/#{string}"] )
        string.sub!(battler_form, "")
      end
       
      return string
    end
	
NOMBRE DE SAUVEGARDES MAXIMALES
Dans Scene_Title l.32
Remplacez :
for i in 0..3
      if FileTest.exist?("Save#{i+1}.rxdata")
        @continue_enabled = true
      end
    end
Par :
1.upto(MAX_SAUVEGARDES) do |i|
      if FileTest.exist?("Save#{i}.rxdata")
        @continue_enabled = true
      end
    end
	
Dans Window_SaveFile, remplacez les : @file_index + 1 par : @file_index

Dans Scene_File l.23 :
Remplacez :
for i in 0..3
Par :
1.upto(MAX_SAUVEGARDES) do |i|

Remplacez les file_index + 1 par file_index (ATTENTION ! Ne touchez pas au @file_index, uniquement les file_index sans e "@" devant !)

l.78 :
Remplacez :
if Input.trigger?(Input::DOWN) or @file_index < 3
Par :
if Input.trigger?(Input::DOWN) or @file_index < MAX_SAUVEGARDES

Dans Scene_Load l. 17 :
Remplacez :
for i in 0..3
Par :
1.upto(MAX_SAUVEGARDES) do |i|

Remplacez $game_temp.last_file_index = i par $game_temp.last_file_index = i - 1

Dans Pokemon_Save l.46 :
Remplacez :
for i in 0..3
		filename = "Save#{i + 1}.rxdata"
        if FileTest.exist?(filename)
          file = File.open(filename, "r")
          if file.mtime > latest_time
            latest_time = file.mtime
            @index = i
          end
          file.close
        end
      end
Par :
1.upto(MAX_SAUVEGARDES) do |i|
        filename = "Save#{i}.rxdata"
        if FileTest.exist?(filename)
          file = File.open(filename, "r")
          if file.mtime > latest_time
            latest_time = file.mtime
            @index = i - 1
          end
          file.close
        end
      end
	  
l.13 :
Remplacez :
for i in 0..2
        if FileTest.exist?("Save#{i+1}.rxdata")
          number += 1
        end
      end
	  
Par :
1.upto(MAX_SAUVEGARDES) do |i|
        number += 1 if FileTest.exist?("Save#{i}.rxdata")
      end
	  
l.89 :
Remplacez :
@index += @index == @number ? 0 : @index == 2 ? 0 : 1
Par :
@index += (@index < @number and @index < MAX_SAUVEGARDES - 1) ? 1 : 0	
	
l.212 :
Remplacez :
if @index == @number and @number < 3
Par :
if @index == @number and @number < MAX_SAUVEGARDES

l. 215 :
Remplacez :
elsif @number < 3
Par :
elsif @number < MAX_SAUVEGARDES

Dans Config Panel, en-dessous de module POKEMON_S :
Ajoutez :
# --------------------------------------------------------
  # MAX_SAUVEGARDES
  #   D�termine le nombre maximum de sauvegardes autoris�es
  #   ATTENTION ! Veuillez r�gler cette valeur en d�but de
  #   projet. Si cette valeur est r�gl� � 10 et qu'ensuite
  #   vous la mettez � 3, ceux qu'ont fait 10 sauvegardes
  #   verront 7 de leur sauvegardes innaccessibles !
  # --------------------------------------------------------
  MAX_SAUVEGARDES = 3

FAIRE DEFILER LES MENUS DE SAUVEGARDES
Dans Scene_Title_SG l.321
Remplacez la m�thode def update par :
def update
    if Input.trigger?(Input::DOWN)
      @index += (@index >= @number) ? 0 : 1
      # Mise � jour des d�placements :
      # D�place les cadres vers le haut si le cadre en-dessous est trop bas
      deplacement_descend = @index > 3 ? -80 * (@index - 3) : 0
      refresh_all(deplacement_descend)
    end
    if Input.trigger?(Input::UP)
      @index -= @index == 0 ? 0 : 1
      # Mise � jour des d�placements :
      # D�place les cadres vers le bas si le cadre au-dessus est trop haut
      deplacement_monte = @index > 3 ? -80 * (@index - 3) : 0
      refresh_all(deplacement_monte)
    end
    if Input.trigger?(Input::C)
      case @index
      when @number # Nouveau jeu
        if @number == MAX_SAUVEGARDES
          $game_system.se_play($data_system.decision_se)
          @erase = true
          @index = @number
          @new_game_window.contents.clear
          @new_game_window.contents.draw_text(9,3,548,48,"RECOMMENCER QUELLE PARTIE?")
          # Mise � jour des d�placements :
          # D�place les cadres vers le haut pour afficher de mani�re visible
          # � l'�cran la derni�re sauvegarde
          deplacement_end = @index > 3 ? -80 * (@index - 3) : 0
          refresh_all(deplacement_end)
        else
          command_new_game
        end
      else # Chargement
        if @erase
          command_new_game
        else
          $game_temp = Game_Temp.new
          on_decision("Save#{@index + 1}.rxdata")
        end
      end
    end
    if Input.trigger?(Input::B) and @erase
      $game_system.se_play($data_system.cancel_se)
      @erase = false
      @index = 3
      @new_game_window.contents.clear
      @new_game_window.contents.draw_text(9,3,548,48,"NOUVELLE PARTIE")
      refresh_all
    end
  end
 
 l.372 :
 Remplacez la m�thode def refresh_all par :
 def refresh_all(deplacement = 0)
    for i in 0...@number
      if i < @index
        @save_game_window_list[i].opacity = 128
        @save_game_window_list[i].y = 30 + 83*i + deplacement
        @save_game_window_list[i].height = 80
        @save_game_window_list[i].contents = Bitmap.new(548, 48)
        set_window(@save_game_window_list[i])
        @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "CONTINUER PARTIE "+(i+1).to_s)
      elsif i == @index
        @save_game_window_list[i].opacity = 255
        @save_game_window_list[i].y = 30 + 83*i + deplacement
        @save_game_window_list[i].height = 163
        @save_game_window_list[i].contents = Bitmap.new(548, 131)
        set_window(@save_game_window_list[i])
        @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "CONTINUER PARTIE "+(i+1).to_s)
        @filename = "Save#{i + 1}.rxdata"
        list = read_preview(@filename)
        data = list[0]
        time_sec = list[1]
        hour = (time_sec / 3600).to_s
        minute = "00"
        minute += ((time_sec%3600)/60).to_s
        minute = minute[minute.size-2, 2]
        time =  hour + ":" + minute
        name = data[0].to_s
        id = data[1].to_i
        captured = data[2].to_s
        @save_game_window_list[i].contents.draw_text(9, 42, 252, 48, "NOM:")
        @save_game_window_list[i].contents.draw_text(9, 42, 252, 48, name, 2)
        @save_game_window_list[i].contents.draw_text(285, 42, 252, 48, "ID:")
        @save_game_window_list[i].contents.draw_text(285, 42, 252, 48, sprintf("%05d",id) , 2)
        @save_game_window_list[i].contents.draw_text(9, 81, 252, 48, "POK�DEX:")
        @save_game_window_list[i].contents.draw_text(9, 81, 252, 48, captured, 2)
        @save_game_window_list[i].contents.draw_text(285, 81, 252, 48, "DUREE:")
        @save_game_window_list[i].contents.draw_text(285, 81, 252, 48, time, 2)
      elsif i > @index
        @save_game_window_list[i].opacity = 128
        @save_game_window_list[i].y = 30 + 83*i + 83 + deplacement
        @save_game_window_list[i].height = 80
        @save_game_window_list[i].contents = Bitmap.new(548, 48)
        set_window(@save_game_window_list[i])
        @save_game_window_list[i].contents.draw_text(9, 3, 548, 48, "CONTINUER PARTIE "+(i+1).to_s)
      end
    end
    if @index == @number
      @new_game_window.opacity = 255
      @new_game_window.y = 30 + 83*@number + deplacement
    else
      @new_game_window.opacity = 128
      @new_game_window.y = 30 + 83*(@number+1) + deplacement
    end
  end
  
Dans Pokemon_Save L.63:
Remplacez la m�thode def main par :
def main
      if @number > 0
        @index = @number - 1
        # Mise � jour des d�placements :
        # D�placement tous les cadres vers le haut de mani�re � pouvoir bien voir
        # � l'�cran la derni�re sauvegarde
        position_deplacement_cadres = @index > 2 ? -80 * (@index - 2) : 0
        refresh_all(position_deplacement_cadres)
      else
        refresh_all
      end
      @background = Spriteset_Map.new
      Graphics.transition
      @text_window = create_text
      @text_window.visible = false
      loop do
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
      Graphics.freeze
      @save_game_window_list.each { |window| window.dispose }
      @text_window.dispose
      @new_save_window.dispose
      @background.dispose
    end
	
l.89 :
Remplacez la m�thode def update par :
 def update
      @background.update if SAVEBOUNDSLOT
      
      if Input.trigger?(Input::DOWN) and not SAVEBOUNDSLOT
        @index += (@index < @number and @index < MAX_SAUVEGARDES - 1) ? 1 : 0
        # Mise � jour des d�placements :
        # D�place les cadres vers le haut si le cadre en-dessous est trop bas
        deplacement_descend = @index > 2 ? -80 * (@index - 2) : 0
        refresh_all(deplacement_descend)
      end
      
      if Input.trigger?(Input::UP) and not SAVEBOUNDSLOT
        @index -= @index == 0 ? 0 : 1
        # Mise � jour des d�placements :
        # D�place les cadres vers le haut si le cadre en-dessous est trop bas
        deplacement_monte = @index > 2 ? -80 * (@index - 2) : 0
        refresh_all(deplacement_monte)
      end
      
      if Input.trigger?(Input::C)
        if @index == @number # Nouvelle sauvegarde
          $game_system.se_play($data_system.save_se)
          filename = "Save#{@number + 1}.rxdata"
          file = File.open(filename, "wb")
          write_save_data(file)
          file.close
          # En cas d'appel d'un �v�nement
          if $game_temp.save_calling
            # Effacer l'indicateur de sauvegarde de l'appel
            $game_temp.save_calling = false
            # Passer � l'�cran de la carte
            $scene = Scene_Map.new
            return
          end
          # Passer � l'�cran de menu
          $scene = POKEMON_S::Pokemon_Menu.new(4)
        else
          decision = draw_choice
          if decision
            $game_system.se_play($data_system.save_se)
            filename = "Save#{@index + 1}.rxdata"
            file = File.open(filename, "wb")
            write_save_data(file)
            file.close
            # En cas d'appel d'un �v�nement
            if $game_temp.save_calling
              # Effacer l'indicateur de sauvegarde de l'appel
              $game_temp.save_calling = false
              # Passer � l'�cran de la carte
              $scene = Scene_Map.new
              return
            end
            # Passer � l'�cran de menu
            $scene = POKEMON_S::Pokemon_Menu.new(4)
          else
            return
          end
        end
        draw_text("La sauvegarde a bien �t� effectu�e !")
        loop do
          Graphics.update
          Input.update
          @text_window.update
          break if Input.trigger?(Input::C)
        end
      end
      
      if Input.trigger?(Input::B)
        if $game_temp.save_calling
          $game_temp.save_calling = false
        end
        $scene = POKEMON_S::Pokemon_Menu.new(4)
        return
      end
      
    end
	
l.166 :
Remplacez la m�thode def refresh_all par :
def refresh_all(deplacement = 0)
      for i in 0...@number
        if i < @index
          @save_game_window_list[i].opacity = 128
          @save_game_window_list[i].y = 30 + 83*i + deplacement
          @save_game_window_list[i].height = 80
          @save_game_window_list[i].contents = Bitmap.new(548, 48)
          set_window(@save_game_window_list[i])
          @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "SAUVEGARDER PARTIE "+(i+1).to_s)
          if SAVEBOUNDSLOT
            @save_game_window_list[i].visible = false
          end
        elsif i == @index
          @save_game_window_list[i].opacity = 255
          @save_game_window_list[i].y = 30 + 83*i + deplacement
          @save_game_window_list[i].height = 163
          @save_game_window_list[i].contents = Bitmap.new(548, 131)
          set_window(@save_game_window_list[i])
          if SAVEBOUNDSLOT
            @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "SAUVEGARDER PARTIE")
            @save_game_window_list[i].y = 30
          else
            @save_game_window_list[i].contents.draw_text(9, 3, 530, 48, "SAUVEGARDER PARTIE "+(i+1).to_s)
          end
          @filename = "Save#{i + 1}.rxdata"
          list = read_preview(@filename)
          data = list[0]
          time_sec = list[1]
          hour = (time_sec / 3600).to_s
          minute = "00"
          minute += ((time_sec%3600)/60).to_s
          minute = minute[minute.size-2, 2]
          time =  hour + ":" + minute
          name = data[0].to_s
          id = data[1].to_s
          captured = data[2].to_s
          @save_game_window_list[i].contents.draw_text(9, 42, 252, 48, "NOM:")
          @save_game_window_list[i].contents.draw_text(9, 42, 252, 48, name, 2)
          @save_game_window_list[i].contents.draw_text(285, 42, 252, 48, "ID:")
          @save_game_window_list[i].contents.draw_text(285, 42, 252, 48, id, 2)
          @save_game_window_list[i].contents.draw_text(9, 81, 252, 48, "POK�DEX:")
          @save_game_window_list[i].contents.draw_text(9, 81, 252, 48, captured, 2)
          @save_game_window_list[i].contents.draw_text(285, 81, 252, 48, "DUREE:")
          @save_game_window_list[i].contents.draw_text(285, 81, 252, 48, time, 2)
        elsif i > @index
          @save_game_window_list[i].opacity = 128
          @save_game_window_list[i].y = 30 + 83*i + 83 + deplacement
          @save_game_window_list[i].height = 80
          @save_game_window_list[i].contents = Bitmap.new(548, 48)
          set_window(@save_game_window_list[i])
          @save_game_window_list[i].contents.draw_text(9, 3, 548, 48, "SAUVEGARDER PARTIE "+(i+1).to_s)
          if SAVEBOUNDSLOT
            @save_game_window_list[i].visible = false
          end
        end
      end
      if @index == @number and @number < MAX_SAUVEGARDES
        @new_save_window.opacity = 255
        @new_save_window.y = 30 + 83*@number + deplacement
      elsif @number < MAX_SAUVEGARDES
        @new_save_window.opacity = 128
        @new_save_window.y = 30 + 83*(@number+1) + deplacement
      else
        @new_save_window.visible = false
      end
    end
	