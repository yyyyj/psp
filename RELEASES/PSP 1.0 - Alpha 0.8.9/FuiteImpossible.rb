APRES RESTRUCT V2

dépendance : aucune

Dans Pokemon_Battle_Core, l.313 :
Supprimez :
$game_system.se_play($data_system.buzzer_se)

Dans Pokemon_Battle_Trainer, l.481 :
Remplacez
def flee_able(actor, enemy)
      if @run
        return super
      end
      @action_window.visible = true
      return false
    end
	  
Par :
def flee_able(actor, enemy)
      if @run
        return super
      end
      draw_text("Fuite impossible", "lors d'un combat de dresseur !")
      wait(40)
      Audio.se_play("Audio/SE/#{$data_audio_se[:phase_joueur]}")
      return false
    end