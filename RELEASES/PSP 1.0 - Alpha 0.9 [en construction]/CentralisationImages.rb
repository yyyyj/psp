APRES RESTRUCT V2

d�pendance : Distributeur.rb

Inidication : Quand il y a la mention de "Remplacez", veuillez comprendre : "Remplacez tous ceux qui existent dans le script"

Dans Config_Panel :
Supprimez :
# --------------------------------------------------------
  # MSG
  #   - Fichier image de la bo�te de dialogue du jeu
  #     dans le dossier Graphics/picture
  # --------------------------------------------------------
  MSG = "messagedummy.png"
  
  # --------------------------------------------------------
  # BATTLE_MSG
  #   - Fond de la fen�tre de message au combat
  #     dans le dossier Graphics/Pictures
  # --------------------------------------------------------
  BATTLE_MSG = "dummy1.png"
  
Dans Scene_Title_SG :
Remplacez :
RPG::Cache.title("Opening2.jpg") Par RPG::Cache.title($data_title[:opening1])
RPG::Cache.title("Opening3.png") Par RPG::Cache.title($data_title[:opening2])
RPG::Cache.title("Opening4.png") Par RPG::Cache.title($data_title[:opening3])
RPG::Cache.title("Opening1.png") Par RPG::Cache.title($data_title[:opening4])
RPG::Cache.title("Opening6.png") Par RPG::Cache.title($data_title[:opening5])
RPG::Cache.title("Opening7.png") Par RPG::Cache.title($data_title[:opening6])
RPG::Cache.title("Opening8.png") Par RPG::Cache.title($data_title[:opening7])
RPG::Cache.title("Opening5.png") Par RPG::Cache.title($data_title[:opening8])
RPG::Cache.title("Opening10.png") Par RPG::Cache.title($data_title[:opening9])
RPG::Cache.title("Opening11.png") Par RPG::Cache.title($data_title[:opening10])
RPG::Cache.picture("fondsaved.png") Par RPG::Cache.picture($data_title[:fond_menu])

Dans MAPLINK / Pokemon_Box / Battle_Core_Interface et Battle_Wild_Animation :
Remplacez les "black.png" par ARRIERE_PLAN

Dans Window_Pokemon / Pokemon_Party_Window / Pokemon_Battle_Status et Carte_Dresseur :
Remplacez les : "Femaleb.png" par FEMELLE
et les "Maleb.png" par MALE

Dans Pokemon_Status / Pokemon_Skill_Learn / Pokemon_Skill_Selection / Pokemon_Box_Status et Panth�on des Dresseurs :
Remplacez les : "Female.png" par FEMELLE
et les "Male.png" par MALE

Dans Pokemon_Battle_Status :
Remplacez : "Femalesb.png" par FEMELLE

Dans Pokemon_Box_Status et Pokemon_Status :
Remplacez : "shiny_interface.PNG" par SHINY_INTERFACE

Dans Pokemon_Party_Window et Pokemon_Battle_Status :
Remplacez : "hpbar.png" par HP_BARRE
Remplacez : "hpbarsmall.png" par HP_BARRE_SMALL

Dans Pokemon_Status / Pokemon_Skill_Learn et Pokemon_Skill_Selection :
Remplacez : "skillselected.png" par SKILL_SELECTIONNE
Remplacez : "skillselection.png" par SKILL_SELECTION

Dans Pokemon_Skill_Learn et Pokemon_Skill_Selection :
Remplacez : "MenuSkill.png" par MENU_SKILL

Dans Pokemon_Menu :
Remplacez :
RPG::Cache.picture("Interface Menu_1.png") Par RPG::Cache.picture($data_menu[:interface_fille]) 
RPG::Cache.picture("Interface Menu_2.png") Par RPG::Cache.picture($data_menu[:interface_garcon])

Dans Pokemon_Party_Menu :
Remplacez :
RPG::Cache.picture("Partyfond.png") Par RPG::Cache.picture($data_menu[:interface_equipe])

Dans Pokemon_Party_Window :
Remplacez :
RPG::Cache.picture("cadretetem.png") Par RPG::Cache.picture($data_menu[:cadre_premier_pokemon_selection])
---------
RPG::Cache.picture("cadretetedl.png") : RPG::Cache.picture("cadretetel.png")]) 
	Par :
RPG::Cache.picture($data_menu[:cadre_premier_pokemon_hover]) : 
                                  RPG::Cache.picture($data_menu[:cadre_premier_pokemon_ko_hover])
----------
RPG::Cache.picture("cadreteted.png") : RPG::Cache.picture("cadretete.png")
	Par :
RPG::Cache.picture($data_menu[:cadre_premier_pokemon]) : 
                                  RPG::Cache.picture($data_menu[:cadre_premier_pokemon_ko])
---------
RPG::Cache.picture("cadrepartym.png") Par RPG::Cache.picture($data_menu[:cadre_pokemon_selection])
----------
RPG::Cache.picture("cadrepartydl.png") : RPG::Cache.picture("cadrepartyl.png")
	Par :
RPG::Cache.picture($data_menu[:cadre_pokemon_hover]) : 
                                  RPG::Cache.picture($data_menu[:cadre_pokemon_ko_hover])
----------
RPG::Cache.picture("cadrepartyd.png") : RPG::Cache.picture("cadreparty.png")
	Par :
RPG::Cache.picture($data_menu[:cadre_pokemon]) : 
                                  RPG::Cache.picture($data_menu[:cadre_pokemon_ko])
----------
RPG::Cache.picture("item_hold.png") Par RPG::Cache.picture($data_menu[:objet])

Dans Pokemon_Status :
Remplacez :
RPG::Cache.picture("MenuAfond.png") Par RPG::Cache.picture($data_menu[:interface_informations])
RPG::Cache.picture("MenuCfond.png") Par RPG::Cache.picture($data_menu[:interface_skills])
RPG::Cache.picture("MenuDfond.png") Par RPG::Cache.picture($data_menu[:interface_skills_details])
RPG::Cache.picture("MenuBfond.png") Par RPG::Cache.picture($data_menu[:interface_informations_stats])	

Dans Pokemon_Computer :
Remplacez :
RPG::Cache.picture("pc/background.png") Par RPG::Cache.picture($data_pc[:background])
RPG::Cache.picture("pc/select.png") Par RPG::Cache.picture($data_pc[:selection])

Dans Pokemon_Box :
Remplacez :
RPG::Cache.picture("pc/boxarrow.png") Par RPG::Cache.picture($data_pc[:fleche_navigation]) 
RPG::Cache.picture("pc/boxbackc.png") Par RPG::Cache.picture($data_pc[:arriere_plan_boite]) 
RPG::Cache.picture("pc/boxleft.png") Par RPG::Cache.picture($data_pc[:arriere_plan_equipe]) 
Graphics.transition(20, "Graphics/Transitions/computertr.png") Par Graphics.transition(20, "Graphics/Transitions/#{$data_pc[:computer_open]}") 
Graphics.transition(20, "Graphics/Transitions/computertrclose.png") Par Graphics.transition(20, "Graphics/Transitions/#{$data_pc[:computer_close]}") 

Dans Pokemon_Computer_Item_Retiring :
Remplacez :
RPG::Cache.picture("pc/item_retire.png") Par RPG::Cache.picture($data_pc[:background_retire])
RPG::Cache.picture("pc/icon_poche.png") Par RPG::Cache.picture($data_pc[:icon_sac])
RPG::Cache.picture("pc/arrow_item.png") Par RPG::Cache.picture($data_pc[:fleche_sac])

Dans Pokemon_Computer_Item_Stock et Pokemon_Item_Bag :
Remplacez :
"background_sac_fille" Par $data_sac[:background_fille]
"background_sac_gars" Par $data_sac[:background_gars]
"bag_fille" Par $data_sac[:icon_sac_fille]
"bag_gars" Par $data_sac[:icon_sac_gars]
RPG::Cache.picture("bag/" + background_name + ".png") Par RPG::Cache.picture("bag/#{background_name}")
RPG::Cache.picture("bag/" + bag_name + ".png") Par RPG::Cache.picture("bag/#{bag_name}")
RPG::Cache.picture("bag/arrow_item.png") Par RPG::Cache.picture($data_sac[:navigation])

Dans Pokemon_Shop_Buy et Pokemon_Shop_Buy_Distrib
Remplacez :
RPG::Cache.picture("shopfond.png") Par : RPG::Cache.picture($data_shop[:fond_shop])

Dans Pokemon_Battle_Status :
Remplacez :
RPG::Cache.picture("battle_sprite1.png") Par RPG::Cache.picture($data_battle[:fenetre_status_enemy])
RPG::Cache.picture("ballbattlestatus.png") Par RPG::Cache.picture($data_battle[:ball_fenetre_status])
RPG::Cache.picture("battle_sprite2.png") Par RPG::Cache.picture($data_battle[:fenetre_status])

