APRES RESTRUCT V2

d�pendance : aucune

Dans Gestion_Switches_Variables :
Ajoutez :
SWITCH_VERIF = 100 # V�rification pour l'apprentissage des capacit�s

et :
# ------------------------------------------------------------
#     Ev�nements commun
# ------------------------------------------------------------
EVENT_COMMUN_CAPACITE = 40 # Event commun de r�apprentissage d'une capacit�

Puis cr�ez un script et ajoutez le script suivant (il ne reste plus qu � faire l event commun, il sera fait pour la sortie de PSP 1.0) :
#==============================================================================
# � Reapprendre_Capacite - Louro
#-----------------------------------------------------------------------------
=begin
SWITCH_VERIF et EVENT_COMMUN_CAPACITE doivent �tre utilis�s ensembles, ils 
servent � utiliser un syst�me de v�rification pour savoir si une attaque a �t� r�apprise, 
ou si �a a �t� annul�, afin de faire une r�action diff�rente. Ce syst�me appelle 
automatiquement un event commun apres le choix du pokemon / fenetre reapprentissage
Si vous voulez desactivez ce syst�me, mettez false � VERIFICATION = false

A propos de SWITCH_VERIF : choisissez un num�ro d'interupteur si 100 ne vous va pas.
 Il se peut que vous fassiez un vendeur qui demande par exemple 100$ contre une attaque
 r�apprise. Mais le joueur peut faire X/Echap (annulation) et ne rien apprendre, bien qu'ayant
 deja pay� l'argent, dans votre evenement. Ce switch servira � r�gler ce probl�me.
 
 Le EVENT_COMMUN_CAPACITE contient le num�ro ID de l'event commun appel� apr�s reapprentissage
 ou annulation de l'apprentissage ou annulation lors du choix du pokemon.
Dans cet event commun, vous utiliserez une condition avec le num�ro de l'interupteur de
SWITCH_VERIF, ici c'est 100 :

Si interupteur  100 est activ�
Message: Bravo, ton pokemon a appris une attaque !
Sinon
Message: Tu as chang� d'avis apparemment.
Argent +100
fin

Vous pourriez vouloir plusieurs message possible dans cet event, ou plusieurs actions possibles
visant � terminer l'event coup� de votre activateur de reapprentissage.
Pour cela je peux vous donnez un exemple :
Avant votre appel par insertion de script du reapprentissage, changez par exemple
la variable 70 en 1 si le gars fait payer 100$, ou en 2 si il demande un objet
Puis dans votre event commun, vous engloberez la condition de verification d'apprentissage
dans une condition avec la variable 70. Si elle est egale � 1 alors ceci, si elle est egale � 2 alros cela..
=end
class Scene_Reapprendre

  def initialize(actor_index = 0)
    @actor_index = actor_index    
  end
  
  def main
    if $game_variables[4] == -1
      $scene = Scene_Map.new
      $game_switches[SWITCH_VERIF] = false
      if VERIFICATION == true then $game_temp.common_event_id = EVENT_COMMUN_CAPACITE end
    end
    if $game_variables[4] != -1
      $game_switches[SWITCH_VERIF] = false
      @actor = $pokemon_party.actors[$game_variables[4]]
      @debug1 = []
      @debug2 = []
      for skill_connu in @actor.skills_set
        @debug1.push skill_connu
      end
      @skill_window = Window_Reapprendre.new(@actor)
      @help_window = Window_Help.new
      if FOND == true
        @fond = Sprite.new
        @fond.bitmap = RPG::Cache.picture(FOND_NOM)
      end
      @skill_window.help_window = @help_window
      @skill_window.opacity = 0
      Graphics.transition
      loop do
        Graphics.update
        Input.update
        update
        if $scene != self
          break
        end
      end
      Graphics.freeze
      @skill_window.dispose
      @help_window.dispose
      $help_window2.visible = false
      if FOND == true then @fond.dispose end
    end
  end

  def update
    @skill_window.update
    @skill_window.refresh
    @help_window.update
    if @skill_window.active
      update_skill
      return
    end
  end
  
  def update_skill
    @skill_window.refresh
    if Input.trigger?(Input::B)
      $game_system.se_play($data_system.cancel_se)
      for skill_connu in @actor.skills_set
        @debug2.push skill_connu
      end
      if @debug1 != @debug2
        $game_switches[SWITCH_VERIF] = true
      else
        $game_switches[SWITCH_VERIF] = false
      end
      $scene = Scene_Map.new
      if VERIFICATION == true then $game_temp.common_event_id = EVENT_COMMUN_CAPACITE end
      return
    end
    if Input.trigger?(Input::C)
      @skill = @skill_window.skill
      if not(@actor.skill_learnt?(@skill.id))
        scene = POKEMON_S::Pokemon_Skill_Learn.new(@actor, @skill.id)
        scene.main
        if INFINI == false
          $scene = Scene_Map.new
        else
          return scene.return_data
        end
      end
      @skill_window.refresh
      $game_system.se_play($data_system.decision_se)
      return
    end
  end
end

class Interpreter
  def reapprendre_attaque
    $scene = Scene_Reapprendre.new
  end
end
    
    
class Window_Reapprendre < Window_Selectable

  def initialize(actor)
    super(0, 128 + 60, 640, 352)
    @actor = actor
    @column_max = 2
    self.index = 0
    refresh
  end
  
  def skill
    return @data[self.index]
  end
  
  def refresh
    if self.contents != nil
      self.contents.dispose
      self.contents = nil
    end
    @data = []      
    for skill in @actor.skills_table
      learning = RPG::Class::Learning.new
      learning.level = skill[1]
      learning.skill_id = skill[0]
      unless @data.include?($data_skills[skill[0]])
        if learning.level <= @actor.level
          if not (@actor.skill_learnt?(skill[0]))
            @data.push $data_skills[skill[0]]
          end
        end
      end
    end
    @item_max = @data.size
    if @item_max > 0
      self.contents = Bitmap.new(width - 32, row_max * 32)
      self.contents.font.name = $fontface
      self.contents.font.size = $fontsize
      for i in 0...@item_max 
        draw_item(i)
      end
    end
  end
  
  def draw_item(index)
    skill = @data[index]
    x = 4 + index % 2 * (288 + 32)
    y = index / 2 * 32
    rect = Rect.new(x, y, self.width / @column_max - 32, 32)
    self.contents.fill_rect(rect, Color.new(0, 0, 0, 0))
    opacity = 255
    if FOND == true
      self.contents.font.color = Color.new(0,0,0,255)
      self.contents.draw_text(x + 28-1, y-1, 204, 32, skill.name, 0)
      self.contents.draw_text(x + 28+1, y+1, 204, 32, skill.name, 0)
      self.contents.draw_text(x + 28+1, y-1, 204, 32, skill.name, 0)
      self.contents.draw_text(x + 28-1, y+1, 204, 32, skill.name, 0)
    end
    self.contents.font.color = Color.new(255,255,255,255)
    self.contents.draw_text(x + 28, y, 204, 32, skill.name, 0)
  end

  def update_help
    for comp2 in @actor.skills_table
      if $data_skills[comp2[0]].name == skill.name
        @help_window.set_text(self.skill == nil ? "" : $data_skills[comp2[0]].description)
        $num0 = comp2[0]
      end
    end
    if $help_window2 != nil then $help_window2.dispose end
    $help_window2 = Window_InfoApp.new
  end
end

class Window_InfoApp < Window_Base

  def initialize
    super(0, 60, 640, 100)
    self.contents = Bitmap.new(width - 32, height - 32)
    self.contents.font.name = $fontface
    self.contents.font.size = $fontsize
    self.contents.font.color = Color.new(0,0,0,255)
    liste
  end
  
  def liste
    self.contents.clear
    skil = $data_skills[$num0]
          src_rect = Rect.new(0, 0, 96, 42)
    bitmap = RPG::Cache.picture("T" + skil.element_set.to_s + ".png")
    self.contents.blt(0, 13, bitmap, src_rect, 255)
    pouv = skil.atk_f + skil.eva_f
    prec = skil.hit
    self.contents.draw_text(120, 17, self.width - 40, 32, "Pouvoir : " + pouv.to_s)
    self.contents.draw_text(300, 17, self.width - 40, 32, "Pr�cis. : " + prec.to_s)
    self.contents.draw_text(480, 17, self.width - 40, 32, "PP : " + skil.sp_cost.to_s)
  end

  def set_text(text, align = 0)
    if text != @text or align != @align
      self.contents.clear
      self.contents.font.color = normal_color
      self.contents.draw_text(4, 0, self.width - 40, 32, text, align)
      @text = text
      @align = align
      @actor = nil
    end
    self.visible = true
  end
end