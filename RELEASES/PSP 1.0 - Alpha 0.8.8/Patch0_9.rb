# Veuillez ex�cuter le script qu'une seule fois.
# Pour cela cr�ez un �v�nement et appellez en script : save_ability
class Interpreter
  def save_ability
    # Rectification dans l'�quipe
    $pokemon_party.actors.each do |pokemon|
      talent = $data_classes[pokemon.id].armor_set.find do |armure| 
        armure == (pokemon.ability+33) 
      end
      # Si ne trouve pas alors le talent est d�cal�
      if talent == nil
        pokemon.ability += 1
      end
    end

    $data_storage.each do |box|
      box.each do |pokemon|
        if pokemon != nil and defined? pokemon.ability
          talent = $data_classes[pokemon.id].armor_set.find do |armure| 
            armure == (pokemon.ability+33) 
          end
          # Si ne trouve pas alors le talent est d�cal�
          if talent == nil
            pokemon.ability += 1
          end
        end
      end
    end
    print "Le script c'est bien ex�cut�, n'oubliez pas de le supprimer."
  end
end