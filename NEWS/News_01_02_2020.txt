Sujet de la news : PSP 1.0 - Alpha 0.8.10 + note d'informations + PSP 1.0 - Alpha 0.9 à venir
Date de la news : 01/02/2020

Nouvelle release qui inclut :
- Patch correctif des attaques appellées avant le début d'un round
- Patch correctif des attaques à ligotement
- Possibilité de ne pas mettre des transitions avant de débuter un combat

Note :
Il s'agit de la 1ere news d'un mois de février. Pour les mois précédent, les news sont archivés dans des dossiers
(comme c'est le cas pour "Janvier 2020").
Si vous installez les mises à jour via le launcher, il se peut que vous ayez le dossier "Janvier 2020", mais
vous aurez également les fichiers news en-dehors du dossier. Pensez à supprimer les différents fichiers textes
des mois précédents qui se trouvent en-dehors de leur dossier pour ne pas vous perdre ;)

PSP 1.0 - Apha 0.9 [en construction], modules disponibles : 
- Mise en place d'un distributeur avec le même fonctionnement qu'un magasin
- Possibilité de saisir les nom des dresseurs et les surnoms des pokemon au clavier [en construction] (démarche manuelle non disponible)